#pragma once

#include "ofMain.h"
#include "ofxGui.h"
class ofApp : public ofBaseApp{

	public:
		void setup();
		void update();
        void draw();
    
    
        ofSpherePrimitive orbs[1000];
        ofEasyCam cam;
        ofLight light1, light2;
    float angle;
    int radius;
    ofxPanel gui;
    ofxIntSlider circleResolution;
		
};
