#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
    
    gui.setup();
    gui.add(circleResolution.setup("circle radius",5, 3, 90));
    angle = 0.0;
    radius = 0;
    
    //Camera Position
    cam.setPosition(100, 100, 300);
    
    //setup a warm light
    light1.setDiffuseColor(ofColor(255, 250, 200));
    light1.setPosition(-100, 100, 500);
    light1.enable();
    
    //setup a cool light
    light2.setDiffuseColor(ofColor(155, 150, 230));
    light2.setPosition(-400, -200, 000);
    light2.enable();
}

//--------------------------------------------------------------
void ofApp::update(){
    
    angle ++;
    
    if(circleResolution){
        radius = circleResolution;
    }
    
    
}

//--------------------------------------------------------------
void ofApp::draw(){
    
    
    ofEnableDepthTest(); //sort the drawing so that the things closest to the camera is in front
    cam.begin();
    ofRotateDeg(angle, 0 + angle, 1 + angle, 0 + angle); //rotate the cube 45 degrees on the y-axis
    ofSetColor(ofRandom(0,255),ofRandom(0,255),ofRandom(0,255));
    for(int i = 0; i<500; i++){
        ofNoFill();
        
        ofRotateDeg(45 , 40, 100, 3);
        ofDrawSphere(i, i, i, radius);
        
    }
    cam.end();
    ofDisableDepthTest();
    
    cam.draw(); //draw whatever the camera saw
    gui.draw();
}

